from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

from .validators import MinAgeValidator


class User(AbstractUser):
    min_age_validator = MinAgeValidator(settings.USER_MIN_AGE)

    birthday = models.DateField(validators=[min_age_validator])
