# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-07 20:22
from __future__ import unicode_literals

import apps.users.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20170807_2008'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='birthday',
            field=models.DateField(null=True, validators=[apps.users.validators.MinAgeValidator(18)]),
        ),
    ]
