from datetime import date
from django.core.validators import BaseValidator


class MinAgeValidator(BaseValidator):
    message = 'Ensure the age is greater than or equal to %(limit_value)s.'

    def compare(self, a, b):
        return a <= b

    def clean(self, x):
        today = date.today()
        total_age = today.year - x.year - ((today.month, today.day) < (x.month, x.day))
        return total_age
