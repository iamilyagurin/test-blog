from django.db.models import Count
from rest_framework import viewsets, permissions
from .serializers import UserSerializer
from ..models import User
from apps.access.permissions import HasAccess


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = (HasAccess,)
    serializer_class = UserSerializer
    queryset = User.objects.annotate(post_count=Count('post'))


