from rest_framework.serializers import (
    HyperlinkedModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField
)

from ..models import User

class UserSerializer(HyperlinkedModelSerializer):
    post_count = SerializerMethodField()

    posts_url = HyperlinkedIdentityField(view_name='post-list', lookup_url_kwarg='uid')
    class Meta:
        model = User
        fields = [
            'id',
            'url',
            'username',
            'birthday',
            'post_count',
            'posts_url',
        ]

    def get_post_count(self, obj):
        return getattr(obj, 'post_count', obj.post_set.count())