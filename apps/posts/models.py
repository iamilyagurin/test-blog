from django.db import models
from django.conf import settings
from .validators import words_validator, not_future


class Post(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)
    title = models.CharField(max_length=255)
    content = models.TextField(validators=[words_validator])
    date = models.DateField(validators=[not_future])
