
import datetime
import string

from django.conf import settings
from django.core.exceptions import ValidationError


def words_validator(text):
    table = str.maketrans({key: None for key in string.punctuation})
    words = set(text.translate(table).lower().split())
    intersection = words.intersection(settings.FORBIDDEN_WORDS)
    if intersection:
        raise ValidationError('Content can not contain words: {0}'.format(', '.join(intersection)))


def not_future(date):
    if date > datetime.date.today():
        raise ValidationError('The date cannot be in the future')
