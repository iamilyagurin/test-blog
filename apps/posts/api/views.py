
from .serializers import PostSerializer, PostSearchSerializer
from rest_framework import viewsets
from ..models import Post
from rest_framework.generics import ListAPIView
from apps.access.permissions import HasAccess
from rest_framework.exceptions import NotFound
from apps.users.models import User


class PostViewSet(viewsets.ModelViewSet):
    permission_classes = (HasAccess,)
    serializer_class = PostSerializer
    queryset = Post.objects.all()

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(owner=self.get_user())
        return qs

    def perform_create(self, serializer):
        serializer.save(owner=self.get_user())

    def get_user(self):
        uid = self.kwargs.get('uid')
        try:
            user = User.objects.get(pk=uid)
        except User.DoesNotExist:
            raise NotFound()
        return user


class PostSearchAPIView(ListAPIView):
    permission_classes = (HasAccess,)
    serializer_class = PostSearchSerializer
    queryset = Post.objects.all().select_related('owner')
    search_fields = ['title']

    def get_queryset(self):
        qs = super().get_queryset()
        qs = self.search_filter(qs=qs)
        return qs

    def search_filter(self, qs):
        get_params = self.request.GET
        kwargs = {'{0}__icontains'.format(f):get_params[f] for f in self.search_fields if get_params.get(f)}
        return qs.filter(**kwargs)
