from rest_framework.serializers import ModelSerializer, SerializerMethodField
from apps.users.api.serializers import UserSerializer
from django.shortcuts import reverse
from ..models import Post


class PostSerializer(ModelSerializer):
    url = SerializerMethodField()
    class Meta:
        model = Post
        fields = [
            'id',
            'title',
            'content',
            'date',
            'url',
        ]

    def get_url(self, obj):
        url = reverse('post-detail', kwargs={'uid': obj.owner.pk, 'pk':obj.pk})
        return url


class PostSearchSerializer(ModelSerializer):
    owner = UserSerializer()
    url = SerializerMethodField()

    class Meta:
        model = Post
        fields = [
            'id',
            'title',
            'content',
            'date',
            'url',
            'owner',
        ]

    def get_url(self, obj):
        url = reverse('post-detail', kwargs={'uid': obj.owner.pk, 'pk':obj.pk})
        return url