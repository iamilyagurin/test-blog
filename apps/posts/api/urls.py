from django.conf.urls import url

from .views import PostViewSet, PostSearchAPIView

post_list = PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

post_detail = PostViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

post_search = PostSearchAPIView.as_view()

urlpatterns = [
    url(r'^(?P<uid>[0-9]+)/posts$', post_list, name='post-list'),
    url(r'^(?P<uid>[0-9]+)/posts/(?P<pk>[0-9]+)$', post_detail, name='post-detail'),
]
