import binascii
import os
from django.db import models


class AccessToken(models.Model):
    key = models.CharField(max_length=50, primary_key=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.key

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(20)).decode()
