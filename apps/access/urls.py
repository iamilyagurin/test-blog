
from django.conf.urls import url
from .views import GetTokenAPIView


urlpatterns = [
    url(r'$', GetTokenAPIView.as_view()),
]
