from rest_framework.views import APIView
from rest_framework.status import HTTP_200_OK
from rest_framework.response import Response
from .models import AccessToken

class GetTokenAPIView(APIView):
    def get(self, request, *args, **kwargs):
        new_token = AccessToken.objects.create()

        return Response({'token': new_token.key}, status=HTTP_200_OK)
