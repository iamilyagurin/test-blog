from rest_framework.permissions import BasePermission
from .models import AccessToken

def get_token(request):
    token_header_name = 'HTTP_ACCESS_TOKEN'
    return request.META.get(token_header_name, '')

def valid_token(key):
    try:
        AccessToken.objects.get(key=key)
    except AccessToken.DoesNotExist:
        return False
    return True

class HasAccess(BasePermission):

    def has_permission(self, request, view):
        key = get_token(request)
        return valid_token(key=key)