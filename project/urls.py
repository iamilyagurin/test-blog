
from django.conf.urls import url, include
from django.contrib import admin

from apps.posts.api.urls import post_search

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^users/', include("apps.users.api.urls")),
    url(r'^user/', include("apps.posts.api.urls")),
    url(r'^search$', post_search, name='post-search'),
    url(r'^token', include('apps.access.urls'))
]
